package com.test.testlauncher;

import android.graphics.drawable.Drawable;

/**
 * Created by gabriel.bl on 12/18/2017.
 */

public class AppDetail {

    public Drawable icon;
    public CharSequence label;
    public String packageName;

}
