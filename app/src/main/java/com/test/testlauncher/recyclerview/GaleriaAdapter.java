package com.test.testlauncher.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.testlauncher.AppDetail;
import com.test.testlauncher.R;
import com.test.testlauncher.recyclerview.ViewHolder.AppViewHolder;
import com.test.testlauncher.recyclerview.ViewHolder.GridViewHolder;
import com.test.testlauncher.recyclerview.ViewHolder.ListViewHolder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GaleriaAdapter extends RecyclerView.Adapter<AppViewHolder> {

    protected static final int GRID_VIEW = 0;
    protected static final int LIST_VIEW = 1;

    private List<AppDetail> apps;
    private int itemSize;
    private boolean gridView;

    public GaleriaAdapter(Collection<AppDetail> apps) {
        this.apps = new ArrayList<>(apps);
    }

    @Override
    public AppViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        AppViewHolder viewHolder = null;
        if (viewType == GRID_VIEW) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.recyclerview_item_grid, viewGroup, false);
            viewHolder = new GridViewHolder(view);
            ((GridViewHolder) viewHolder).setItemSize(itemSize);
        } else if (viewType == LIST_VIEW) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.recyclerview_item_list, viewGroup, false);
            viewHolder = new ListViewHolder(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AppViewHolder viewHolder, final int position) {
        viewHolder.onBindData(apps.get(position));
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (ehGridView()) {
            return GRID_VIEW;
        } else {
            return LIST_VIEW;
        }
    }

    public void setItemSize(int itemSize) {
        this.itemSize = itemSize;
    }

    public void setGridView(boolean gridView) {
        this.gridView = gridView;
    }

    public boolean ehGridView() {
        return gridView;
    }

}
