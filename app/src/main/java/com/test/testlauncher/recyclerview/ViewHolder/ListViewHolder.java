package com.test.testlauncher.recyclerview.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.test.testlauncher.AppDetail;
import com.test.testlauncher.R;

/**
 * Created by gabriel.bl on 12/18/2017.
 */

public class ListViewHolder extends AppViewHolder {

    private TextView appName;
    private ImageView imgView;
    private TextView appPackage;

    public ListViewHolder(View itemView) {
        super(itemView);
        imgView = itemView.findViewById(R.id.imgView);
        appName = itemView.findViewById(R.id.appName);
        appPackage = itemView.findViewById(R.id.appPackage);
    }

    public void onBindData(AppDetail appDetail) {
        super.onBindData(appDetail);
        appName.setText(appDetail.label);
        imgView.setImageDrawable(appDetail.icon);
        appPackage.setText(appDetail.packageName);
    }

}
