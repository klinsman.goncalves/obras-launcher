package com.test.testlauncher.recyclerview.ViewHolder;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.test.testlauncher.AppDetail;

/**
 * Created by gabriel.bl on 12/18/2017.
 */

public abstract class AppViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    protected AppDetail appDetail;

    public AppViewHolder(View itemView) {
        super(itemView);
    }

    public void onBindData(AppDetail appDetail) {
        this.appDetail = appDetail;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Context context = itemView.getContext();
        Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(appDetail.packageName);
        context.startActivity(intent);
    }

}
