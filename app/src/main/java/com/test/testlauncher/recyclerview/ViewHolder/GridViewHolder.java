package com.test.testlauncher.recyclerview.ViewHolder;

import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.test.testlauncher.AppDetail;
import com.test.testlauncher.R;

/**
 * Created by gabriel.bl on 12/18/2017.
 */

public class GridViewHolder extends AppViewHolder {

    private ImageView imgView;
    private TextView txtView;

    public GridViewHolder(View itemView) {
        super(itemView);
        txtView = itemView.findViewById(R.id.txtView);
        imgView = itemView.findViewById(R.id.imgView);
    }

    public void onBindData(AppDetail appDetail) {
        super.onBindData(appDetail);
        txtView.setText(appDetail.label);
        imgView.setImageDrawable(appDetail.icon);
    }

    public void setItemSize(final int itemSize) {
        itemView.setLayoutParams(new FrameLayout.LayoutParams(itemSize, itemSize));
        // É preciso pois o Height do txtView ainda não foi calculado.
        // Verificar essa resposta do StackOverflow: https://goo.gl/bFvZCj
        txtView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        txtView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        int imgSize = itemSize - (txtView.getHeight() * 2);
                        imgView.getLayoutParams().height = imgSize;
                        imgView.getLayoutParams().width = imgSize;
                        //imgView.setPadding(imgView.getPaddingLeft(), imgView.getPaddingTop(),
                        //        imgView.getPaddingRight(), txtView.getHeight());
                        imgView.requestLayout();
                    }
                }
        );
    }
}
