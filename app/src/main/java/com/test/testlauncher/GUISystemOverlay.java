package com.test.testlauncher;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ViewGroup;

/**
 * Created by josiel.s on 20-Dec-17.
 */

public class GUISystemOverlay extends ViewGroup {


    public GUISystemOverlay(Context context) {
        super(context);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.d("customViewGroup", "**********TRYING OPEN STATUS BAR");
        return true;
    }

}
