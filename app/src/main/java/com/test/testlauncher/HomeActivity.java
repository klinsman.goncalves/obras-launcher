package com.test.testlauncher;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.test.testlauncher.Services.ServiceBlockStatusBar;
import com.test.testlauncher.recyclerview.GaleriaAdapter;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    private Set<String> pacotesRequeridos;
    private Set<AppDetail> appsEncontrados;
    private FloatingActionButton changeViewButton;
    private GaleriaAdapter galeriaAdapter;
    private RecyclerView appsRecyclerView;
    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager linearLayoutManager;
    public static final int mOverelay_Permission = 10101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        changeViewButton = findViewById(R.id.changeView);
        changeViewButton.setOnClickListener(this);

        configuraPacotesRequeridos();
        buscaAppsInstalados();

        appsRecyclerView = findViewById(R.id.rv_apps);
        appsRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                configuraGridView(appsRecyclerView);
            }
        });
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());

        if (checkDrawOverlayPermission()) {
            Toast.makeText(getApplicationContext(), "Hello World!", Toast.LENGTH_SHORT).show();
            startService(new Intent(getBaseContext(), ServiceBlockStatusBar.class));
        }
    }

    @Override
    public void onBackPressed() {

        Toast.makeText(getApplicationContext(), R.string.on_back_pressed, Toast.LENGTH_SHORT).show();
    }

    private void configuraPacotesRequeridos() {
        pacotesRequeridos = new HashSet<>();
        pacotesRequeridos.add(getResources().getString(R.string.app_saude));
        pacotesRequeridos.add(getResources().getString(R.string.app_educa));
        pacotesRequeridos.add(getResources().getString(R.string.app_mobi));
        pacotesRequeridos.add(getResources().getString(R.string.app_segam));
        pacotesRequeridos.add(getResources().getString(R.string.app_obras));
        pacotesRequeridos.add(getResources().getString(R.string.app_saude_admin));
        pacotesRequeridos.add(getResources().getString(R.string.app_educa_admin));
        pacotesRequeridos.add(getResources().getString(R.string.app_mobi_admin));
        pacotesRequeridos.add(getResources().getString(R.string.app_segam_admin));
        pacotesRequeridos.add(getResources().getString(R.string.app_obras_admin));
    }

    private void buscaAppsInstalados() {
        if (appsEncontrados == null) {
            appsEncontrados = new HashSet<>();
        }

        PackageManager packageManager = getPackageManager();
        List<ApplicationInfo> apps = packageManager.getInstalledApplications(
                PackageManager.GET_META_DATA);

        AppDetail appDetail;
        for (ApplicationInfo appInfo : apps) {
            if (pacotesRequeridos.contains(appInfo.packageName)) {
                appDetail = new AppDetail();
                appDetail.icon = appInfo.loadIcon(packageManager);
                appDetail.label = appInfo.loadLabel(packageManager);
                appDetail.packageName = appInfo.packageName;
                appsEncontrados.add(appDetail);
            }
        }
    }

    public void configuraGridView(RecyclerView recyclerView) {
        int width = recyclerView.getMeasuredWidth();
        final int DEFAULT_SIZE = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 80, getResources().getDisplayMetrics());
        int itemSize = DEFAULT_SIZE;

        // Calcula numero de colunas
        int nItens = width / itemSize;
        itemSize += (width - itemSize * nItens) / nItens;
        int padding = (width % nItens) / 2;

        // Ajusta padding para deixar centralizado
        int topPadding = recyclerView.getPaddingTop();
        int bottonPadding = recyclerView.getPaddingBottom();
        recyclerView.setPadding(padding, topPadding, padding, bottonPadding);

        // Cria layout manager
        gridLayoutManager = new GridLayoutManager(recyclerView.getContext(), nItens);
        recyclerView.setLayoutManager(gridLayoutManager);

        // Ajusta adapter
        galeriaAdapter = new GaleriaAdapter(appsEncontrados);
        galeriaAdapter.setItemSize(itemSize);
        recyclerView.setAdapter(galeriaAdapter);
        ativaGridView();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.changeView) {
            galeriaAdapter.setGridView(!galeriaAdapter.ehGridView());
            if (galeriaAdapter.ehGridView()) {
                ativaGridView();
            } else {
                ativaListView();
            }
        }
    }

    private void ativaGridView() {
        galeriaAdapter.setGridView(true);
        galeriaAdapter.notifyDataSetChanged();
        appsRecyclerView.setLayoutManager(gridLayoutManager);
        changeViewButton.setImageResource(R.drawable.ic_view_list_white_24dp);
    }

    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, mOverelay_Permission);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onResume() {
        if (checkDrawOverlayPermission()) {
            startService(new Intent(getBaseContext(), ServiceBlockStatusBar.class));
        }
        super.onResume();
    }

    @Override
    @TargetApi(Build.VERSION_CODES.M)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == mOverelay_Permission) {
            if (Settings.canDrawOverlays(this)) {
                startService(new Intent(this, HomeActivity.class));
            }
        }
    }

    private void ativaListView() {
        galeriaAdapter.setGridView(false);
        galeriaAdapter.notifyDataSetChanged();
        appsRecyclerView.setLayoutManager(linearLayoutManager);
        changeViewButton.setImageResource(R.drawable.ic_view_module_white_24dp);
    }

}
